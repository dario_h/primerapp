//
//  ViewController.swift
//  BullsEye
//
//  Created by Dario Herrera on 24/4/18.
//  Copyright © 2018 Dario Herrera. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    


    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    
    @IBOutlet weak var gameSlider: UISlider!
    
    var target = 0
    var score = 0
    var roundGame = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        restarGame()
        
    }
    
   
    @IBAction func playButtonPressed(_ sender: Any) {
        let sliderValue = Int(gameSlider.value)
        
        switch sliderValue {
        case target:
            score += 100
        case (target - 2)...(target + 2):
            score += 50
        case (target - 5)...(target + 5):
            score += 10
        default:
            break
        }
        
        roundGame += 1
        roundLabel.text = "\(roundGame)"
        target = Int(arc4random_uniform(100))
        scoreLabel.text = "\(score)"
        targetLabel.text = "\(target)"
        

        
    }
    
    @IBAction func restartButtonPressed(_ sender: Any) {
        restarGame()
        
    }
    
    func restarGame(){
        roundLabel.text = "1"
        score = 0
        target = Int(arc4random_uniform(100))
        targetLabel.text = "\(target)"
        scoreLabel.text = "\(score)"
    }
    
    @IBAction func infoButtonPressed(_ sender: Any) {
    }
    
    @IBAction func WinnerButtonPressed(_ sender: Any) {
        
        if score > 100 {
            performSegue(withIdentifier: "toWinnerSegue", sender: self)
        }
    }
    

}

